package com.gcp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CapsgcppocApplication {

	public static void main(String[] args) {
		SpringApplication.run(CapsgcppocApplication.class, args);
	}

}
